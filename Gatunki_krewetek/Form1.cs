﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Gatunki_krewetek
{
    public partial class Form1 : Form
    {
        string kategoriaID = "0";
        public string nazwaKrewetki, nazwaLacinska, wystepowanie, twardoscWody, pokarm, dlugosc, temperatura, pH, zbiornik, idKrewetki;
        public string selectedKrewetka;

        public Form1()
        {
            InitializeComponent();
            loadKrewetki();
            panel1.Visible = false;
            populateEditCombobox();
            populateDeleteCombobox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadKrewetki();
        }

        public void loadKrewetki()
        {
            DataSet dataSet = new DataSet();
            dataSet.ReadXml("./krewetki.xml");
            dataGridView1.DataSource = dataSet.Tables[0];
        }

        public void refreshPopulateCombobox()
        {
            DataSet dataSet = new DataSet();
            dataSet.ReadXml("./krewetki.xml");
            editNameComboBox.DataSource = dataSet.Tables[0];
        }

        public void addKrewetki()
        {
            XDocument dok = new XDocument();
            dok = XDocument.Load("./krewetki.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load("./krewetki.xml");

            XmlNodeList nodelistKrewetki = doc.DocumentElement.SelectNodes("/lista/krewetka");
            int iloscKrewetek = nodelistKrewetki.Count;
            try
            {
                XElement root = new XElement("krewetka");
                root.Add(new XAttribute("id_krewetki", iloscKrewetek + 1));
                root.Add(new XAttribute("id_kategorii", kategoriaID));
                root.Add(new XElement("nazwa", nameTextBox.Text));
                root.Add(new XElement("nazwa_lacinska", latinNameTextBox.Text));
                root.Add(new XElement("wystepowanie", appreanceTextBox.Text));
                root.Add(new XElement("dlugosc", widthTextBox.Text));
                root.Add(new XElement("temperatura", tempTextBox.Text));
                root.Add(new XElement("twardosc_wody", waterHardnessTextBox.Text));
                root.Add(new XElement("pH", pHTextBox.Text));
                root.Add(new XElement("zbiornik", tankSizeTextBox.Text));
                root.Add(new XElement("pokarm", foodTextBox.Text));

                dok.Element("lista").Add(root);
                dok.Save("./krewetki.xml");

                MessageBox.Show("Krewetka została dodana!",
                                             "Powodzenie dodania",
                                             MessageBoxButtons.OK);
                populateDeleteCombobox();
                populateEditCombobox();
                loadKrewetki();
            }
            catch
            {
                MessageBox.Show("Popraw dane!", "Niepowodzenie dodania", MessageBoxButtons.RetryCancel);
            }
        }


        private void addButton_Click(object sender, EventArgs e)
        {
            int temp;

            nazwaKrewetki = nameTextBox.Text;
            nazwaLacinska = latinNameTextBox.Text;
            wystepowanie = appreanceTextBox.Text;
            twardoscWody = waterHardnessTextBox.Text;
            pokarm = foodTextBox.Text;

            if (int.TryParse(widthTextBox.Text, out temp))
            {
                dlugosc = widthTextBox.Text;
            }
            if (int.TryParse(tempTextBox.Text, out temp))
            {
                temperatura = tempTextBox.Text;
            }
            if (int.TryParse(tankSizeTextBox.Text, out temp))
            {
                zbiornik = tankSizeTextBox.Text;
            }
            if (int.TryParse(pHTextBox.Text, out temp))
            {
                pH = pHTextBox.Text;
            }
            addKrewetki();
        }

        public void editKrewetki()
        {
            panel1.Visible = true;
            XDocument dok = new XDocument();
            dok = XDocument.Load("./krewetki.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load("./krewetki.xml");

            XmlNodeList nodeListKrewetki = doc.DocumentElement.SelectNodes("/lista/krewetka");

            int iloscKrewetek = nodeListKrewetki.Count;

            for (int i = 0; i < iloscKrewetek; i++)
            {
                XmlNode node = nodeListKrewetki.Item(i);
                if (node.SelectSingleNode("nazwa").InnerText == selectedKrewetka)
                {
                    idKrewetki = node.Attributes["id_krewetki"].InnerText;
                    kategoriaID = node.Attributes["id_kategorii"].InnerText;
                    nazwaKrewetki = node.SelectSingleNode("nazwa").InnerText;
                    nazwaLacinska = node.SelectSingleNode("nazwa_lacinska").InnerText;
                    wystepowanie = node.SelectSingleNode("wystepowanie").InnerText;
                    dlugosc = node.SelectSingleNode("dlugosc").InnerText;
                    temperatura = node.SelectSingleNode("temperatura").InnerText;
                    twardoscWody = node.SelectSingleNode("twardosc_wody").InnerText;
                    pH = node.SelectSingleNode("pH").InnerText;
                    zbiornik = node.SelectSingleNode("zbiornik").InnerText;
                    pokarm = node.SelectSingleNode("pokarm").InnerText;

                    editCategoryComboBox.Text = kategoriaID;
                    editLatinNameTextBox.Text = nazwaLacinska;
                    editApperanceTextBox.Text = wystepowanie;
                    editWidthTextBox.Text = dlugosc;
                    editTempTextBox.Text = temperatura;
                    editWaterHardnessTextBox.Text = twardoscWody;
                    editPhTextBox.Text = pH;
                    editTankSizeTextBox.Text = zbiornik;
                    editFoodTextBox.Text = pokarm;

                }
            }
        }

        private void editAddButton_Click(object sender, EventArgs e)
        {
            editKrewetki();
            XmlDocument doc = new XmlDocument();
            doc.Load("./krewetki.xml");
            XmlNodeList nodeListKrewetki = doc.DocumentElement.SelectNodes("/lista/krewetka");

            int iloscKrewetek = nodeListKrewetki.Count;

            for (int i = 0; i < iloscKrewetek; i++)
            {
                XmlNode node = nodeListKrewetki.Item(i);
                if (node.Attributes["id_krewetki"].InnerText == idKrewetki)
                    node.ParentNode.RemoveChild(node);
                doc.Save("./krewetki.xml");
            }

            XDocument dok = new XDocument();
            dok = XDocument.Load("./krewetki.xml");

            XElement root = new XElement("krewetka");
            root.Add(new XAttribute("id_krewetki", idKrewetki));
            root.Add(new XAttribute("id_kategorii", editCategoryComboBox.SelectedIndex + 1));
            root.Add(new XElement("nazwa", editNameComboBox.Text));
            root.Add(new XElement("nazwa_lacinska", editLatinNameTextBox.Text));
            root.Add(new XElement("wystepowanie", editApperanceTextBox.Text));
            root.Add(new XElement("dlugosc", editWidthTextBox.Text));
            root.Add(new XElement("temperatura", editTempTextBox.Text));
            root.Add(new XElement("twardosc_wody", editWaterHardnessTextBox.Text));
            root.Add(new XElement("pH", editPhTextBox.Text));
            root.Add(new XElement("zbiornik", editTankSizeTextBox.Text));
            root.Add(new XElement("pokarm", editFoodTextBox.Text));

            dok.Element("lista").Add(root);
            dok.Save("./krewetki.xml");

            MessageBox.Show("Krewetka została zaktualizowana", "Powodzenie aktualizacji", MessageBoxButtons.OK);
            editKrewetki();
            loadKrewetki();
            populateEditCombobox();
            populateDeleteCombobox();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();

            doc.Load("./krewetki.xml");

            XmlNodeList nodelistKrewetki = doc.DocumentElement.SelectNodes("/lista/krewetka");
            int iloscKrewetek = nodelistKrewetki.Count;
            for (int i = 0; i < iloscKrewetek; i++)
            {
                XmlNode node = nodelistKrewetki.Item(i);
                if (node.SelectSingleNode("nazwa").InnerText == deleteItemComboBox.Text)
                node.ParentNode.RemoveChild(node);
                doc.Save("./krewetki.xml");
            }
            MessageBox.Show("Krewetka została usunięta", "Powodzenie usunięcia", MessageBoxButtons.OK);
            populateDeleteCombobox();
            populateEditCombobox();
            loadKrewetki();
        }

        public void populateEditCombobox()
        {
            loadKrewetki();
            DataSet dsCategories = new DataSet();
            dsCategories.ReadXml("krewetki.xml");
            //dsCategories.Tables[0].DefaultView.Sort = "Nazwa";
            editNameComboBox.DataSource = dsCategories.Tables[0];
            editNameComboBox.DisplayMember = "nazwa";
            editNameComboBox.ValueMember = "nazwa";
        }
        public void populateDeleteCombobox()
        {
            loadKrewetki();
            DataSet dsCategories = new DataSet();
            dsCategories.ReadXml("krewetki.xml");
            //dsCategories.Tables[0].DefaultView.Sort = "Nazwa";
            deleteItemComboBox.DataSource = dsCategories.Tables[0];
            deleteItemComboBox.DisplayMember = "nazwa";
            deleteItemComboBox.ValueMember = "nazwa";
        }
 

        private void editNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedKrewetka = editNameComboBox.Text;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            loadKrewetki();
            editKrewetki();
        }

        private void gatunekComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            kategoriaID = Convert.ToString(gatunekComboBox.SelectedIndex + 1);
        }

        private void editCategoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            kategoriaID = Convert.ToString(editCategoryComboBox.SelectedIndex + 1);
        }
    }
}
