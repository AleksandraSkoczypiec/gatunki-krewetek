<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:variable name="KrewetkiTable">
	<tr bgcolor="#02e683">
		<th style="text-align:left"><a href="JavaScript:reSort('nazwa')">Nazwa</a></th>
		<th style="text-align:left"><a href="JavaScript:reSort('nazwa_lacinska')">Nazwa lacinska</a></th>
		<th style="text-align:left">Wystepowanie</th>
		<th style="text-align:left">Dlugosc</th>
		<th style="text=align:left">Temperatura</th>
		<th style="text=align:left">Twardosc wody</th>
		<th style="text=align:left">pH</th>
		<th style="text=align:left">Zbiornik</th>
		<th style="text=align:left">Pokarm</th>
	</tr>
</xsl:variable>

<xsl:param name="sortKey">nazwa</xsl:param>
<xsl:param name="sortOrder">ascending</xsl:param>
<xsl:param name="sortType">text</xsl:param>

	<xsl:template match="/">
		<html> 
			<body id="disp">
				<script type="text/javascript" src="sortXML.js"></script> 
				<h2>Zbior krewetek</h2>
				<table border="2">
					<xsl:copy-of select="$KrewetkiTable"/>

					<xsl:for-each select="document('krewetki.xml')/lista/krewetka">
					<xsl:sort select="*[name(.)=$sortKey]|@*[name(.)=$sortKey]" order="{$sortOrder}" data-type="{$sortType}"/>
						<tr>
							<td><xsl:value-of select="nazwa"/></td>
							<td><xsl:value-of select="nazwa_lacinska"/></td>
							<td><xsl:value-of select="wystepowanie"/></td>
							<td><xsl:value-of select="dlugosc"/></td>
							<td><xsl:value-of select="temperatura"/></td>
							<td><xsl:value-of select="twardosc_wody"/></td>
							<td><xsl:value-of select="pH"/></td>
							<td><xsl:value-of select="zbiornik"/></td>
							<td><xsl:value-of select="pokarm"/></td>
						</tr>
					</xsl:for-each>
				</table>
				  
				<table border="2" id="RodzinaTable">
					<tr bgcolor="#02e683">
						<th style="text-align:left">Nazwa</th>
						<th style="text-align:left">Opis</th>
	
					</tr>

					<xsl:for-each select="document('rodzina.xml')/kategorie/kategoria">
						<tr>
							<td><xsl:value-of select="nazwa"/></td>
							<td><xsl:value-of select="opis"/></td>
							
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>