﻿namespace Gatunki_krewetek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showKrewetkiControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.gatunekComboBox = new System.Windows.Forms.ComboBox();
            this.addButton = new System.Windows.Forms.Button();
            this.foodLabel = new System.Windows.Forms.Label();
            this.foodTextBox = new System.Windows.Forms.TextBox();
            this.tankSizeLabel = new System.Windows.Forms.Label();
            this.tankSizeTextBox = new System.Windows.Forms.TextBox();
            this.pHLabel = new System.Windows.Forms.Label();
            this.pHTextBox = new System.Windows.Forms.TextBox();
            this.waterHardnessLabel = new System.Windows.Forms.Label();
            this.waterHardnessTextBox = new System.Windows.Forms.TextBox();
            this.temperatureLabel = new System.Windows.Forms.Label();
            this.tempTextBox = new System.Windows.Forms.TextBox();
            this.widthLabel = new System.Windows.Forms.Label();
            this.widthTextBox = new System.Windows.Forms.TextBox();
            this.apperanceLabel = new System.Windows.Forms.Label();
            this.appreanceTextBox = new System.Windows.Forms.TextBox();
            this.latinNameLabel = new System.Windows.Forms.Label();
            this.latinNameTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.editButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.editCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.editAddButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.editFoodTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.editTankSizeTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.editPhTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.editWaterHardnessTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.editTempTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.editWidthTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.editApperanceTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.editLatinNameTextBox = new System.Windows.Forms.TextBox();
            this.editNameComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.deleteItemComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.showKrewetkiControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // showKrewetkiControl
            // 
            this.showKrewetkiControl.Controls.Add(this.tabPage1);
            this.showKrewetkiControl.Controls.Add(this.tabPage2);
            this.showKrewetkiControl.Controls.Add(this.tabPage3);
            this.showKrewetkiControl.Controls.Add(this.tabPage4);
            this.showKrewetkiControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.showKrewetkiControl.Location = new System.Drawing.Point(0, 0);
            this.showKrewetkiControl.Name = "showKrewetkiControl";
            this.showKrewetkiControl.SelectedIndex = 0;
            this.showKrewetkiControl.Size = new System.Drawing.Size(1126, 539);
            this.showKrewetkiControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1118, 513);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Przeglądaj krewetki";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1112, 507);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.categoryLabel);
            this.tabPage2.Controls.Add(this.gatunekComboBox);
            this.tabPage2.Controls.Add(this.addButton);
            this.tabPage2.Controls.Add(this.foodLabel);
            this.tabPage2.Controls.Add(this.foodTextBox);
            this.tabPage2.Controls.Add(this.tankSizeLabel);
            this.tabPage2.Controls.Add(this.tankSizeTextBox);
            this.tabPage2.Controls.Add(this.pHLabel);
            this.tabPage2.Controls.Add(this.pHTextBox);
            this.tabPage2.Controls.Add(this.waterHardnessLabel);
            this.tabPage2.Controls.Add(this.waterHardnessTextBox);
            this.tabPage2.Controls.Add(this.temperatureLabel);
            this.tabPage2.Controls.Add(this.tempTextBox);
            this.tabPage2.Controls.Add(this.widthLabel);
            this.tabPage2.Controls.Add(this.widthTextBox);
            this.tabPage2.Controls.Add(this.apperanceLabel);
            this.tabPage2.Controls.Add(this.appreanceTextBox);
            this.tabPage2.Controls.Add(this.latinNameLabel);
            this.tabPage2.Controls.Add(this.latinNameTextBox);
            this.tabPage2.Controls.Add(this.nameLabel);
            this.tabPage2.Controls.Add(this.nameTextBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1118, 513);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Dodaj krewetkę";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label12.Location = new System.Drawing.Point(481, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(146, 25);
            this.label12.TabIndex = 21;
            this.label12.Text = "Dodaj krewetkę";
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point(388, 108);
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(55, 13);
            this.categoryLabel.TabIndex = 20;
            this.categoryLabel.Text = "Kategoria:";
            // 
            // gatunekComboBox
            // 
            this.gatunekComboBox.FormattingEnabled = true;
            this.gatunekComboBox.Items.AddRange(new object[] {
            "Neocaridina",
            "Caridina",
            "Atyopsis"});
            this.gatunekComboBox.Location = new System.Drawing.Point(449, 103);
            this.gatunekComboBox.Name = "gatunekComboBox";
            this.gatunekComboBox.Size = new System.Drawing.Size(226, 21);
            this.gatunekComboBox.TabIndex = 19;
            this.gatunekComboBox.SelectedIndexChanged += new System.EventHandler(this.gatunekComboBox_SelectedIndexChanged);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(536, 394);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 18;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // foodLabel
            // 
            this.foodLabel.AutoSize = true;
            this.foodLabel.Location = new System.Drawing.Point(397, 341);
            this.foodLabel.Name = "foodLabel";
            this.foodLabel.Size = new System.Drawing.Size(46, 13);
            this.foodLabel.TabIndex = 17;
            this.foodLabel.Text = "Pokarm:";
            // 
            // foodTextBox
            // 
            this.foodTextBox.Location = new System.Drawing.Point(449, 338);
            this.foodTextBox.Name = "foodTextBox";
            this.foodTextBox.Size = new System.Drawing.Size(226, 20);
            this.foodTextBox.TabIndex = 16;
            // 
            // tankSizeLabel
            // 
            this.tankSizeLabel.AutoSize = true;
            this.tankSizeLabel.Location = new System.Drawing.Point(350, 315);
            this.tankSizeLabel.Name = "tankSizeLabel";
            this.tankSizeLabel.Size = new System.Drawing.Size(93, 13);
            this.tankSizeLabel.TabIndex = 15;
            this.tankSizeLabel.Text = "Rozmiar zbiornika:";
            // 
            // tankSizeTextBox
            // 
            this.tankSizeTextBox.Location = new System.Drawing.Point(449, 312);
            this.tankSizeTextBox.Name = "tankSizeTextBox";
            this.tankSizeTextBox.Size = new System.Drawing.Size(226, 20);
            this.tankSizeTextBox.TabIndex = 14;
            // 
            // pHLabel
            // 
            this.pHLabel.AutoSize = true;
            this.pHLabel.Location = new System.Drawing.Point(419, 289);
            this.pHLabel.Name = "pHLabel";
            this.pHLabel.Size = new System.Drawing.Size(24, 13);
            this.pHLabel.TabIndex = 13;
            this.pHLabel.Text = "pH:";
            // 
            // pHTextBox
            // 
            this.pHTextBox.Location = new System.Drawing.Point(449, 286);
            this.pHTextBox.Name = "pHTextBox";
            this.pHTextBox.Size = new System.Drawing.Size(226, 20);
            this.pHTextBox.TabIndex = 12;
            // 
            // waterHardnessLabel
            // 
            this.waterHardnessLabel.AutoSize = true;
            this.waterHardnessLabel.Location = new System.Drawing.Point(358, 263);
            this.waterHardnessLabel.Name = "waterHardnessLabel";
            this.waterHardnessLabel.Size = new System.Drawing.Size(85, 13);
            this.waterHardnessLabel.TabIndex = 11;
            this.waterHardnessLabel.Text = "Twardość wody:";
            // 
            // waterHardnessTextBox
            // 
            this.waterHardnessTextBox.Location = new System.Drawing.Point(449, 260);
            this.waterHardnessTextBox.Name = "waterHardnessTextBox";
            this.waterHardnessTextBox.Size = new System.Drawing.Size(226, 20);
            this.waterHardnessTextBox.TabIndex = 10;
            // 
            // temperatureLabel
            // 
            this.temperatureLabel.AutoSize = true;
            this.temperatureLabel.Location = new System.Drawing.Point(345, 237);
            this.temperatureLabel.Name = "temperatureLabel";
            this.temperatureLabel.Size = new System.Drawing.Size(98, 13);
            this.temperatureLabel.TabIndex = 9;
            this.temperatureLabel.Text = "Temperatura wody:";
            // 
            // tempTextBox
            // 
            this.tempTextBox.Location = new System.Drawing.Point(449, 234);
            this.tempTextBox.Name = "tempTextBox";
            this.tempTextBox.Size = new System.Drawing.Size(226, 20);
            this.tempTextBox.TabIndex = 8;
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Location = new System.Drawing.Point(392, 211);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(51, 13);
            this.widthLabel.TabIndex = 7;
            this.widthLabel.Text = "Długość:";
            // 
            // widthTextBox
            // 
            this.widthTextBox.Location = new System.Drawing.Point(449, 208);
            this.widthTextBox.Name = "widthTextBox";
            this.widthTextBox.Size = new System.Drawing.Size(226, 20);
            this.widthTextBox.TabIndex = 6;
            // 
            // apperanceLabel
            // 
            this.apperanceLabel.AutoSize = true;
            this.apperanceLabel.Location = new System.Drawing.Point(363, 185);
            this.apperanceLabel.Name = "apperanceLabel";
            this.apperanceLabel.Size = new System.Drawing.Size(80, 13);
            this.apperanceLabel.TabIndex = 5;
            this.apperanceLabel.Text = "Występowanie:";
            // 
            // appreanceTextBox
            // 
            this.appreanceTextBox.Location = new System.Drawing.Point(449, 182);
            this.appreanceTextBox.Name = "appreanceTextBox";
            this.appreanceTextBox.Size = new System.Drawing.Size(226, 20);
            this.appreanceTextBox.TabIndex = 4;
            // 
            // latinNameLabel
            // 
            this.latinNameLabel.AutoSize = true;
            this.latinNameLabel.Location = new System.Drawing.Point(356, 159);
            this.latinNameLabel.Name = "latinNameLabel";
            this.latinNameLabel.Size = new System.Drawing.Size(87, 13);
            this.latinNameLabel.TabIndex = 3;
            this.latinNameLabel.Text = "Nazwa łacińska:";
            // 
            // latinNameTextBox
            // 
            this.latinNameTextBox.Location = new System.Drawing.Point(449, 156);
            this.latinNameTextBox.Name = "latinNameTextBox";
            this.latinNameTextBox.Size = new System.Drawing.Size(226, 20);
            this.latinNameTextBox.TabIndex = 2;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(400, 133);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(43, 13);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "Nazwa:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(449, 130);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(226, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.editButton);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Controls.Add(this.editNameComboBox);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1118, 513);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Modyfikuj krewetkę";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(676, 106);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(107, 23);
            this.editButton.TabIndex = 44;
            this.editButton.Text = "Edytuj";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.editCategoryComboBox);
            this.panel1.Controls.Add(this.editAddButton);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.editFoodTextBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.editTankSizeTextBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.editPhTextBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.editWaterHardnessTextBox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.editTempTextBox);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.editWidthTextBox);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.editApperanceTextBox);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.editLatinNameTextBox);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(0, 135);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1112, 382);
            this.panel1.TabIndex = 43;
            this.panel1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(374, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 60;
            this.label1.Text = "Kategoria:";
            // 
            // editCategoryComboBox
            // 
            this.editCategoryComboBox.FormattingEnabled = true;
            this.editCategoryComboBox.Items.AddRange(new object[] {
            "Neocaridina",
            "Caridina",
            "Atyopsis"});
            this.editCategoryComboBox.Location = new System.Drawing.Point(435, 3);
            this.editCategoryComboBox.Name = "editCategoryComboBox";
            this.editCategoryComboBox.Size = new System.Drawing.Size(226, 21);
            this.editCategoryComboBox.TabIndex = 59;
            this.editCategoryComboBox.SelectedIndexChanged += new System.EventHandler(this.editCategoryComboBox_SelectedIndexChanged);
            // 
            // editAddButton
            // 
            this.editAddButton.Location = new System.Drawing.Point(507, 270);
            this.editAddButton.Name = "editAddButton";
            this.editAddButton.Size = new System.Drawing.Size(75, 23);
            this.editAddButton.TabIndex = 58;
            this.editAddButton.Text = "Modyfikuj";
            this.editAddButton.UseVisualStyleBackColor = true;
            this.editAddButton.Click += new System.EventHandler(this.editAddButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(383, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Pokarm:";
            // 
            // editFoodTextBox
            // 
            this.editFoodTextBox.Location = new System.Drawing.Point(435, 212);
            this.editFoodTextBox.Name = "editFoodTextBox";
            this.editFoodTextBox.Size = new System.Drawing.Size(226, 20);
            this.editFoodTextBox.TabIndex = 56;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(336, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 55;
            this.label3.Text = "Rozmiar zbiornika:";
            // 
            // editTankSizeTextBox
            // 
            this.editTankSizeTextBox.Location = new System.Drawing.Point(435, 186);
            this.editTankSizeTextBox.Name = "editTankSizeTextBox";
            this.editTankSizeTextBox.Size = new System.Drawing.Size(226, 20);
            this.editTankSizeTextBox.TabIndex = 54;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(405, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "pH:";
            // 
            // editPhTextBox
            // 
            this.editPhTextBox.Location = new System.Drawing.Point(435, 160);
            this.editPhTextBox.Name = "editPhTextBox";
            this.editPhTextBox.Size = new System.Drawing.Size(226, 20);
            this.editPhTextBox.TabIndex = 52;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(344, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Twardość wody:";
            // 
            // editWaterHardnessTextBox
            // 
            this.editWaterHardnessTextBox.Location = new System.Drawing.Point(435, 134);
            this.editWaterHardnessTextBox.Name = "editWaterHardnessTextBox";
            this.editWaterHardnessTextBox.Size = new System.Drawing.Size(226, 20);
            this.editWaterHardnessTextBox.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(331, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 13);
            this.label6.TabIndex = 49;
            this.label6.Text = "Temperatura wody:";
            // 
            // editTempTextBox
            // 
            this.editTempTextBox.Location = new System.Drawing.Point(435, 108);
            this.editTempTextBox.Name = "editTempTextBox";
            this.editTempTextBox.Size = new System.Drawing.Size(226, 20);
            this.editTempTextBox.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(378, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Długość:";
            // 
            // editWidthTextBox
            // 
            this.editWidthTextBox.Location = new System.Drawing.Point(435, 82);
            this.editWidthTextBox.Name = "editWidthTextBox";
            this.editWidthTextBox.Size = new System.Drawing.Size(226, 20);
            this.editWidthTextBox.TabIndex = 46;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(349, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Występowanie:";
            // 
            // editApperanceTextBox
            // 
            this.editApperanceTextBox.Location = new System.Drawing.Point(435, 56);
            this.editApperanceTextBox.Name = "editApperanceTextBox";
            this.editApperanceTextBox.Size = new System.Drawing.Size(226, 20);
            this.editApperanceTextBox.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(342, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "Nazwa łacińska:";
            // 
            // editLatinNameTextBox
            // 
            this.editLatinNameTextBox.Location = new System.Drawing.Point(435, 30);
            this.editLatinNameTextBox.Name = "editLatinNameTextBox";
            this.editLatinNameTextBox.Size = new System.Drawing.Size(226, 20);
            this.editLatinNameTextBox.TabIndex = 42;
            // 
            // editNameComboBox
            // 
            this.editNameComboBox.FormattingEnabled = true;
            this.editNameComboBox.Location = new System.Drawing.Point(435, 106);
            this.editNameComboBox.Name = "editNameComboBox";
            this.editNameComboBox.Size = new System.Drawing.Size(226, 21);
            this.editNameComboBox.TabIndex = 42;
            this.editNameComboBox.SelectedIndexChanged += new System.EventHandler(this.editNameComboBox_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(386, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Nazwa:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.deleteButton);
            this.tabPage4.Controls.Add(this.deleteItemComboBox);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1118, 513);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Usuń krewetkę";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label11.Location = new System.Drawing.Point(419, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(282, 25);
            this.label11.TabIndex = 2;
            this.label11.Text = "Wybierz krewetkę do usunięcia";
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(580, 208);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 1;
            this.deleteButton.Text = "Usuń";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // deleteItemComboBox
            // 
            this.deleteItemComboBox.FormattingEnabled = true;
            this.deleteItemComboBox.Location = new System.Drawing.Point(454, 156);
            this.deleteItemComboBox.Name = "deleteItemComboBox";
            this.deleteItemComboBox.Size = new System.Drawing.Size(201, 21);
            this.deleteItemComboBox.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label13.Location = new System.Drawing.Point(461, 45);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(178, 25);
            this.label13.TabIndex = 45;
            this.label13.Text = "Modyfikuj krewetkę";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 539);
            this.Controls.Add(this.showKrewetkiControl);
            this.Name = "Form1";
            this.Text = "Gatunki krewetek";
            this.showKrewetkiControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl showKrewetkiControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label foodLabel;
        private System.Windows.Forms.TextBox foodTextBox;
        private System.Windows.Forms.Label tankSizeLabel;
        private System.Windows.Forms.TextBox tankSizeTextBox;
        private System.Windows.Forms.Label pHLabel;
        private System.Windows.Forms.TextBox pHTextBox;
        private System.Windows.Forms.Label waterHardnessLabel;
        private System.Windows.Forms.TextBox waterHardnessTextBox;
        private System.Windows.Forms.Label temperatureLabel;
        private System.Windows.Forms.TextBox tempTextBox;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.TextBox widthTextBox;
        private System.Windows.Forms.Label apperanceLabel;
        private System.Windows.Forms.TextBox appreanceTextBox;
        private System.Windows.Forms.Label latinNameLabel;
        private System.Windows.Forms.TextBox latinNameTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.ComboBox gatunekComboBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox editCategoryComboBox;
        private System.Windows.Forms.Button editAddButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox editFoodTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox editTankSizeTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox editPhTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox editWaterHardnessTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox editTempTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox editWidthTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox editApperanceTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox editLatinNameTextBox;
        private System.Windows.Forms.ComboBox editNameComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.ComboBox deleteItemComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
    }
}

